extends Resource

class_name things_list

@export var portrai_list : Array[Texture]
@export var background_list : Array[Texture]
@export var content_background_list : Array[Texture]
@export var music_list : Array[AudioStream]
