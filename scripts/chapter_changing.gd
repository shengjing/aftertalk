extends Control

@onready var chapter_text : Label = $TextureRect/Label
@onready var content_text : Label = $TextureRect/Label2

var typing_tween_change : Tween

var chapter_dic : Dictionary = {
	"0" : "序章",
	"1" : "第一章",
	"2" : "第二章",
	"3" : "第三章",
	"4" : "第四章",
	"5" : "第五章"
}

var content_dic : Dictionary = {
	"0" : "天台、铁丝网以及……",
	"1" : "生死与未来皆无____",
	"2" : "33333",
	"3" : "44444",
	"4" : "55555",
	"5" : "66666"
}

var text_for_chapter : String = chapter_dic[str(SaveAndLoad.chapter_index)]
var text_for_content : String = content_dic[str(SaveAndLoad.chapter_index)]

func _ready():
	await get_tree().create_timer(1.0).timeout
	_content_update1()
	await get_tree().create_timer(0.5).timeout
	_content_update2()
	await get_tree().create_timer(2.5).timeout
	get_tree().change_scene_to_file("res://scene/dialogue_manager.tscn")

func _content_update1():
	typing_tween_change = get_tree().create_tween()
	chapter_text.text = ""
	for character in text_for_chapter:
		typing_tween_change.tween_callback(_append_content.bind(character)).set_delay(0.15)

func _content_update2():
	typing_tween_change = get_tree().create_tween()
	content_text.text = ""
	for character in text_for_content:
		typing_tween_change.tween_callback(_append_content1.bind(character)).set_delay(0.2)

func _append_content(character : String):
	chapter_text.text += character

func _append_content1(character : String):
	content_text.text += character
