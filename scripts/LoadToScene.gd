extends Node

var loading_screen = load("res://scene/loading_ui.tscn")
var loading_scene_path : String

func _change_scene_to(path : String):
	loading_scene_path = path
	get_tree().change_scene_to_packed(loading_screen)
