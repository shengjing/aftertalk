extends Node

var dialogue_index : int = 0
var chapter_index : int = 0

const volume_path : String = "user://volume_data.ini"

var saving_dic : Dictionary = {
	"chapter_saving" : 0,
	"dialogue_saving" : 0
}

func _file1_save():
	var Saving_File1 : FileAccess = FileAccess.open("user://saving_file_1.txt",FileAccess.WRITE)
	var Saving_FileTime1 : FileAccess = FileAccess.open("user://saving_filetime1.txt",FileAccess.WRITE)
	
	saving_dic["chapter_saving"] = SaveAndLoad.chapter_index
	saving_dic["dialogue_saving"] = SaveAndLoad.dialogue_index
	
	Saving_File1.store_var(saving_dic)
	Saving_FileTime1.store_string(Time.get_datetime_string_from_system(false,true))
	
	Saving_File1.close()
	Saving_FileTime1.close()

func _file1_load():
	var Loading_File1 : FileAccess = FileAccess.open("user://saving_file_1.txt",FileAccess.READ)
	
	saving_dic = Loading_File1.get_var()
	
	chapter_index = saving_dic["chapter_saving"]
	dialogue_index = saving_dic["dialogue_saving"]
	
	Loading_File1.close()

func _file1_delete():
	DirAccess.remove_absolute("user://saving_file_1.txt")
	DirAccess.remove_absolute("user://saving_filetime1.txt")

func _file2_save():
	var Saving_File2 : FileAccess = FileAccess.open("user://saving_file_2.txt",FileAccess.WRITE)
	var Saving_FileTime2 : FileAccess = FileAccess.open("user://saving_filetime2.txt",FileAccess.WRITE)
	
	saving_dic["chapter_saving"] = chapter_index
	saving_dic["dialogue_saving"] = dialogue_index
	
	Saving_File2.store_var(saving_dic)
	Saving_FileTime2.store_string(Time.get_datetime_string_from_system(false,true))
	
	Saving_File2.close()
	Saving_FileTime2.close()

func _file2_load():
	var Loading_File2 : FileAccess = FileAccess.open("user://saving_file_2.txt",FileAccess.READ)
	
	saving_dic = Loading_File2.get_var()
	
	chapter_index = saving_dic["chapter_saving"]
	dialogue_index = saving_dic["dialogue_saving"]
	
	Loading_File2.close()

func _file2_delete():
	DirAccess.remove_absolute("user://saving_file_2.txt")
	DirAccess.remove_absolute("user://saving_filetime2.txt")

func _file3_save():
	var Saving_File3 : FileAccess = FileAccess.open("user://saving_file_3.txt",FileAccess.WRITE)
	var Saving_FileTime3 : FileAccess = FileAccess.open("user://saving_filetime3.txt",FileAccess.WRITE)
	
	saving_dic["chapter_saving"] = SaveAndLoad.chapter_index
	saving_dic["dialogue_saving"] = SaveAndLoad.dialogue_index
	
	Saving_File3.store_var(saving_dic)
	Saving_FileTime3.store_string(Time.get_datetime_string_from_system(false,true))
	
	Saving_File3.close()
	Saving_FileTime3.close()

func _file3_load():
	var Loading_File3 : FileAccess = FileAccess.open("user://saving_file_3.txt",FileAccess.READ)
	
	saving_dic = Loading_File3.get_var()
	
	chapter_index = saving_dic["chapter_saving"]
	dialogue_index = saving_dic["dialogue_saving"]
	
	Loading_File3.close()

func _file3_delete():
	DirAccess.remove_absolute("user://saving_file_3.txt")
	DirAccess.remove_absolute("user://saving_filetime3.txt")

func _linear_to_db_get(bus_index : int) -> float:
	var db : float = AudioServer.get_bus_volume_db(bus_index)
	return db_to_linear(db)

func _db_to_linear_set(bus_index : int , v : float) -> void :
	var db : int = linear_to_db(v)
	AudioServer.set_bus_volume_db(bus_index,db)

func _save_volume_settings():
	var config_save : ConfigFile = ConfigFile.new()
	
	config_save.set_value("volume","master",_linear_to_db_get(0))
	
	config_save.save("user://volume_data.ini")

func _load_volume_settings():
	var config_load : ConfigFile = ConfigFile.new()
	
	config_load.load("user://volume_data.ini")
	
	if config_load.load("user://volume_data.ini") != OK:
		return
	var volume_things : float = config_load.get_value("volume","master",0.5)
	SaveAndLoad._db_to_linear_set(0,volume_things)

