extends Control

@export_group("UI")
@export var load_background : TextureRect
@export var settings_background : TextureRect
@export var button_start_new : Button
@export var button_load : Button
@export var button_settings : Button
@export var button_list : Button
@export var button_thanks : Button
@export var loadname1 : Label
@export var loadname2 : Label
@export var loadname3 : Label
@export var load_1 : Button
@export var load_2 : Button
@export var load_3 : Button
@export var delete_1 : Button
@export var delete_2 : Button
@export var delete_3 : Button
@export var setting_volume : HSlider

@export_file("*.tscn") var path : String
var load_is_open: bool = false
var settings_is_open : bool = false

func _ready():
	if MusicManager.music_player.playing == true :
		pass
	else :
		MusicManager._play_music()
	
	SaveAndLoad.dialogue_index = 0
	SaveAndLoad.chapter_index = 0
	
	load_background.visible = false
	settings_background.visible = false
	
	_file1_content_update()
	_file2_content_update()
	_file3_content_update()
	
	button_start_new.pressed.connect(_turn_to_dialogue_manager)
	button_load.pressed.connect(_load_open)
	button_settings.pressed.connect(_settings_open)
	button_thanks.pressed.connect(_play_thanks)
	load_1.pressed.connect(_file1_main_load)
	load_2.pressed.connect(_file2_main_load)
	load_3.pressed.connect(_file3_main_load)
	delete_1.pressed.connect(_file1_main_delete)
	delete_2.pressed.connect(_file2_main_delete)
	delete_3.pressed.connect(_file3_main_delete)
	
	SaveAndLoad._load_volume_settings()
	setting_volume.value_changed.connect(func(v : float):
		SaveAndLoad._db_to_linear_set(0,v)
		SaveAndLoad._save_volume_settings())
	setting_volume.value = SaveAndLoad._linear_to_db_get(0)

func _turn_to_dialogue_manager():
	LoadToScene._change_scene_to("res://scene/chapter_changing.tscn")

func _load_open():
	if settings_is_open == true:
		pass
	elif load_background.visible == false:
		load_background.visible = true
		load_is_open = true
		button_load.text = "收回"
	elif load_background.visible == true:
		load_background.visible = false
		load_is_open = false
		button_load.text = "读取存档"

func _settings_open():
	if load_is_open == true:
		pass
	elif settings_background.visible == false:
		settings_background.visible = true
		settings_is_open = true
		button_settings.text = "收回"
	elif settings_background.visible == true:
		settings_background.visible = false
		settings_is_open = false
		button_settings.text = "设置"

func _play_thanks():
	LoadToScene._change_scene_to("res://scene/thanks.tscn")

func _file1_content_update():
	var load1_name : FileAccess = FileAccess.open("user://saving_filetime1.txt",FileAccess.READ)
	
	if FileAccess.file_exists("user://saving_file_1.txt") and FileAccess.file_exists("user://saving_filetime1.txt"):
		loadname1.text = load1_name.get_as_text()
		
		load1_name.close()
	else :
		loadname1.text = "存档1"

func _file2_content_update():
	var load2_name : FileAccess = FileAccess.open("user://saving_filetime2.txt",FileAccess.READ)
	
	if FileAccess.file_exists("user://saving_file_2.txt") and FileAccess.file_exists("user://saving_filetime2.txt"):
		loadname2.text = load2_name.get_as_text()
		
		load2_name.close()
	else :
		loadname2.text = "存档2"

func _file3_content_update():
	var load3_name : FileAccess = FileAccess.open("user://saving_filetime3.txt",FileAccess.READ)
	
	if FileAccess.file_exists("user://saving_file_3.txt") and FileAccess.file_exists("user://saving_filetime3.txt"):
		loadname3.text = load3_name.get_as_text()
		
		load3_name.close()
	else :
		loadname3.text = "存档3"

func _file1_main_load():
	if FileAccess.file_exists("user://saving_file_1.txt") and FileAccess.file_exists("user://saving_filetime1.txt"):
		SaveAndLoad._file1_load()
		_turn_to_dialogue_manager()
	else :
		load_1.text = "当前无存档"
		await get_tree().create_timer(1.0).timeout
		load_1.text = "读取"

func _file2_main_load():
	if FileAccess.file_exists("user://saving_file_2.txt") and FileAccess.file_exists("user://saving_filetime2.txt"):
		SaveAndLoad._file2_load()
		_turn_to_dialogue_manager()
	else :
		load_2.text = "当前无存档"
		await get_tree().create_timer(1.0).timeout
		load_2.text = "读取"

func _file3_main_load():
	if FileAccess.file_exists("user://saving_file_3.txt") and FileAccess.file_exists("user://saving_filetime3.txt"):
		SaveAndLoad._file3_load()
		_turn_to_dialogue_manager()
	else :
		load_3.text = "当前无存档"
		await get_tree().create_timer(1.0).timeout
		load_3.text = "读取"

func _file1_main_delete():
	if FileAccess.file_exists("user://saving_file_1.txt") and FileAccess.file_exists("user://saving_filetime1.txt"):
		delete_1.text = "删除中"
		await get_tree().create_timer(0.7).timeout
		SaveAndLoad._file1_delete()
		delete_1.text = "已删除"
		await get_tree().create_timer(0.5).timeout
		delete_1.text = "删除"
		_file1_content_update()
	else :
		delete_1.text = "当前无存档"
		await get_tree().create_timer(0.7).timeout
		delete_1.text = "删除"

func _file2_main_delete():
	if FileAccess.file_exists("user://saving_file_2.txt") and FileAccess.file_exists("user://saving_filetime2.txt"):
		delete_2.text = "删除中"
		await get_tree().create_timer(0.7).timeout
		SaveAndLoad._file2_delete()
		delete_2.text = "已删除"
		await get_tree().create_timer(0.5).timeout
		delete_2.text = "删除"
		_file2_content_update()
	else :
		delete_2.text = "当前无存档"
		await get_tree().create_timer(0.7).timeout
		delete_2.text = "删除"

func _file3_main_delete():
	if FileAccess.file_exists("user://saving_file_3.txt") and FileAccess.file_exists("user://saving_filetime3.txt"):
		delete_3.text = "删除中"
		await get_tree().create_timer(0.7).timeout
		SaveAndLoad._file3_delete()
		delete_3.text = "已删除"
		await get_tree().create_timer(0.5).timeout
		delete_3.text = "删除"
		_file3_content_update()
	else :
		delete_3.text = "当前无存档"
		await get_tree().create_timer(0.7).timeout
		delete_3.text = "删除"

func _volume_update():
	SaveAndLoad._save_volume_settings()
	setting_volume.value = SaveAndLoad._linear_to_db_get(0)
