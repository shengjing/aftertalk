extends Control

@export var thanks_container : VBoxContainer
@export var tips_label : Label

var rolling_tween : Tween

func _ready():
	tips_label.visible = false

func _process(_delta):
	rolling_tween = get_tree().create_tween()
	
	rolling_tween.tween_callback(_label_roll).set_delay(0.5)

func _label_roll():
	if thanks_container.position.y >= -979:
		thanks_container.position.y -= 1
	else :
		LoadToScene._change_scene_to("res://scene/main_menu.tscn")

func _on_gui_input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT and event.double_click:
		LoadToScene._change_scene_to("res://scene/main_menu.tscn")
	elif event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
		tips_label.visible = true
		await get_tree().create_timer(1.5).timeout
		tips_label.visible = false
