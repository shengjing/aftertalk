extends Control

@export var loading_process : ProgressBar

var scene_path : String
var progress : Array
var update : float = 0.0

func _ready():
	scene_path = LoadToScene.loading_scene_path
	
	ResourceLoader.load_threaded_request(scene_path)

func _process(_delta):
	ResourceLoader.load_threaded_get_status(scene_path,progress)
	
	if progress[0] > update :
		update = progress[0]
	 
	if loading_process.value >= 1.0 :
		if update >= 1.0 :
			await get_tree().create_timer(0.618).timeout
			get_tree().change_scene_to_packed(ResourceLoader.load_threaded_get(scene_path))
	else :
		pass

	if loading_process.value < update :
		loading_process.value += lerp(loading_process.value,update,_delta)
	else :
		pass
	
	loading_process.value += _delta * 0.2 * \
		(0.5 if update >= 1.0 else clamp(0.9 - loading_process.value,0.0,1.0))
	
	loading_process.value = update

