extends Resource

class_name  dialogue_things

@export var character_name : String
@export_multiline var content : String
@export var portrait : Texture
@export var background : Texture

