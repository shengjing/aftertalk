extends Node

var aspect_ratio : float

var temp_size : Vector2i

func _process(_delta : float) -> void :
	if temp_size != get_window().size :
		aspect_ratio = float(get_window().size.x) / float(get_window().size.y)
		if aspect_ratio < 1 :
			get_window().size.x = get_window().size.y
		elif aspect_ratio > 2.334 :
			get_window().size.y = get_window().size.y * 0.429
		temp_size = get_window().size
