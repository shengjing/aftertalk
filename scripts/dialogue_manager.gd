extends Control

@export_group("UI")
@export var character_name_text : Label
@export var text_box : Label
@export var portrait : TextureRect
@export var content_background : TextureRect
@export var background : TextureRect
@export var save_and_load : Button
@export var back_to_menu : Button
@export var menu_2D : VBoxContainer
@export var save : Label
@export var save1 : Label
@export var save2 : Label
@export var save_button_1 : Button
@export var save_button_2 : Button
@export var save_button_3 : Button
@export var save1_button_1 : Button
@export var save1_button_2 : Button
@export var save1_button_3 : Button
@export var save2_button_1 : Button
@export var save2_button_2 : Button
@export var save2_button_3 : Button
@export var volume_change : HSlider

@export_group("对话")
@export var dialogue_now : dialogue_pack#对话
@export var dialogue_list : dialogue_package#
@export var dialogue_thing_for_manager : dialogue_things
@export var dialogue_packs_for_saving : dialogue_pack
@export var dialogue_packages_for_saving : dialogue_package
@export_file("*.tscn") var path : String
#
#var dialogue_state: DialogueState = DialogueState.DialogueFinished

#enum DialogueState{
	#DialoguePlaying, ## 播放中
	#DialogueFinished ## 播放完成
#}

var typing_tween : Tween
var ui_is_open : bool = false

func _ready():
	menu_2D.visible = false
	back_to_menu.visible = false
	portrait.texture = null
	
	save_and_load.pressed.connect(_ui_switch)
	back_to_menu.pressed.connect(_turn_back_to_menu)
	save_button_1.pressed.connect(_file1_save_now)
	save_button_2.pressed.connect(_file1_load_now)
	save_button_3.pressed.connect(_file1_delete_now)
	save1_button_1.pressed.connect(_file2_save_now)
	save1_button_2.pressed.connect(_file2_load_now)
	save1_button_3.pressed.connect(_file2_delete_now)
	save2_button_1.pressed.connect(_file3_save_now)
	save2_button_2.pressed.connect(_file3_load_now)
	save2_button_3.pressed.connect(_file3_delete_now)
	
	_file1_buttontext_update()
	_file2_buttontext_update()
	_file3_buttontext_update()
	
	SaveAndLoad._load_volume_settings()
	volume_change.value_changed.connect(func(v : float):
		SaveAndLoad._db_to_linear_set(0,v)
		SaveAndLoad._save_volume_settings())
	volume_change.value = SaveAndLoad._linear_to_db_get(0)
	
	_to_the_chapter(SaveAndLoad.chapter_index)

func _next_chapter():
	SaveAndLoad.chapter_index += 1
	if _other_chapter():
		dialogue_now = dialogue_list.dialogue_packages[SaveAndLoad.chapter_index]
		SaveAndLoad.dialogue_index = 0
		_chapter_turn()
	else :
		LoadToScene._change_scene_to("res://scene/thanks.tscn")

func _play_next_dia():
	if typing_tween and typing_tween.is_running():
		typing_tween.kill()
		text_box.text = dialogue_thing_for_manager.content
		return
	
	SaveAndLoad.dialogue_index += 1
	
	if not SaveAndLoad.dialogue_index <= dialogue_now.dialogue_packs.size() - 1:
		_next_chapter()
		return
	
	dialogue_thing_for_manager = dialogue_now.dialogue_packs[SaveAndLoad.dialogue_index]
	text_box.text = dialogue_thing_for_manager.content
	
	if dialogue_thing_for_manager.character_name != null :
		character_name_text.text = dialogue_thing_for_manager.character_name
	else :
		pass
	
	portrait.texture = dialogue_thing_for_manager.portrait
	
	typing_tween = get_tree().create_tween()
	text_box.text = ""
	for character in dialogue_thing_for_manager.content:
		typing_tween.tween_callback(_append_content.bind(character)).set_delay(0.05)
	
	if (dialogue_thing_for_manager.background != null):
		background.texture = dialogue_thing_for_manager.background

func _append_content(character : String):
	text_box.text += character

func _on_content_background_gui_input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
		_play_next_dia()
	else :
		pass

func _on_background_gui_input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT and event.pressed and ui_is_open == true:
		_ui_close()
	else :
		pass

func _other_chapter() -> bool:
	if SaveAndLoad.chapter_index <= dialogue_list.dialogue_packages.size() - 1:
		return true
	else :
		return false

func _ui_switch():
	if ui_is_open == false :
		menu_2D.visible = true
		back_to_menu.visible = true
		ui_is_open = true
		save_and_load.text = "收回"
	else :
		_ui_close()

func _ui_close():
	menu_2D.visible = false
	back_to_menu.visible = false
	ui_is_open = false
	save_and_load.text = "菜单"

func _play_dia_now():
	if typing_tween and typing_tween.is_running():
		typing_tween.kill()
		text_box.text = dialogue_thing_for_manager.content
		return
	
	dialogue_thing_for_manager = dialogue_now.dialogue_packs[SaveAndLoad.dialogue_index]
	text_box.text = dialogue_thing_for_manager.content
	
	if dialogue_thing_for_manager.character_name != null :
		character_name_text.text = dialogue_thing_for_manager.character_name
	else :
		pass
	
	portrait.texture = dialogue_thing_for_manager.portrait
	
	typing_tween = get_tree().create_tween()
	text_box.text = ""
	for character in dialogue_thing_for_manager.content:
		typing_tween.tween_callback(_append_content.bind(character)).set_delay(0.05)
	
	if (dialogue_thing_for_manager.background != null):
		background.texture = dialogue_thing_for_manager.background

func _chapter_turn():
	get_tree().change_scene_to_file("res://scene/chapter_changing.tscn")

func _to_the_chapter(_int):
	dialogue_now = dialogue_list.dialogue_packages[SaveAndLoad.chapter_index]
	_play_dia_now()

func _file1_buttontext_update():
	if FileAccess.file_exists("user://saving_file_1.txt") and FileAccess.file_exists("user://saving_filetime1.txt"):
		var file1_time_load : FileAccess = FileAccess.open("user://saving_filetime1.txt",FileAccess.READ)
		
		save.text = file1_time_load.get_as_text()
		save_button_1.text = "覆盖"
		
		file1_time_load.close()
	else :
		save.text = "存档1"
		save_button_1.text = "存档"

func _file2_buttontext_update():
	if FileAccess.file_exists("user://saving_file_2.txt") and FileAccess.file_exists("user://saving_filetime2.txt"):
		var file2_time_load : FileAccess = FileAccess.open("user://saving_filetime2.txt",FileAccess.READ)
		
		save1.text = file2_time_load.get_as_text()
		save1_button_1.text = "覆盖"
		
		file2_time_load.close()
	else :
		save1.text = "存档2"
		save1_button_1.text = "存档"

func _file3_buttontext_update():
	if FileAccess.file_exists("user://saving_file_3.txt") and FileAccess.file_exists("user://saving_filetime3.txt"):
		var file3_time_load : FileAccess = FileAccess.open("user://saving_filetime3.txt",FileAccess.READ)
		
		save2.text = file3_time_load.get_as_text()
		save2_button_1.text = "覆盖"
		
		file3_time_load.close()
	else :
		save2.text = "存档3"
		save2_button_1.text = "存档"

func _turn_back_to_menu():
	LoadToScene._change_scene_to("res://scene/main_menu.tscn")

func _file1_save_now():
	save_button_1.text = "存档中"
	await get_tree().create_timer(1.5).timeout
	SaveAndLoad._file1_save()
	save_button_1.text = "已存档"
	await get_tree().create_timer(1.0).timeout
	_file1_buttontext_update()

func _file1_load_now():
	if FileAccess.file_exists("user://saving_file_1.txt") and FileAccess.file_exists("user://saving_filetime1.txt"):
		save_button_2.text = "读取中"
		await get_tree().create_timer(1.0).timeout
		SaveAndLoad._file1_load()
		_chapter_turn()

	else :
		save_button_2.text = "当前无存档"
		await get_tree().create_timer(1.0).timeout
		save_button_2.text = "读取"

func _file1_delete_now():
	if FileAccess.file_exists("user://saving_file_1.txt") and FileAccess.file_exists("user://saving_filetime1.txt"):
		save_button_3.text = "删除中"
		SaveAndLoad._file1_delete()
		await get_tree().create_timer(0.7).timeout
		save_button_3.text = "已删除"
		_file1_buttontext_update()
		await get_tree().create_timer(0.7).timeout
		save_button_3.text = "删除"
	else :
		save_button_3.text = "当前无存档"
		await get_tree().create_timer(0.7).timeout
		save_button_3.text = "删除"

func _file2_save_now():
	save1_button_1.text = "存档中"
	await get_tree().create_timer(1.5).timeout
	SaveAndLoad._file2_save()
	save1_button_1.text = "已存档"
	await get_tree().create_timer(1.0).timeout
	_file2_buttontext_update()

func _file2_load_now():
	if FileAccess.file_exists("user://saving_file_2.txt") and FileAccess.file_exists("user://saving_filetime2.txt"):
		save1_button_2.text = "读取中"
		await get_tree().create_timer(1.0).timeout
		SaveAndLoad._file2_load()
		_chapter_turn()
	else :
		save1_button_2.text = "当前无存档"
		await get_tree().create_timer(1.0).timeout
		save1_button_2.text = "读取"

func _file2_delete_now():
	if FileAccess.file_exists("user://saving_file_2.txt") and FileAccess.file_exists("user://saving_filetime2.txt"):
		save1_button_3.text = "删除中"
		SaveAndLoad._file2_delete()
		await get_tree().create_timer(0.7).timeout
		save1_button_3.text = "已删除"
		_file2_buttontext_update()
		await get_tree().create_timer(0.7).timeout
		save1_button_3.text = "删除"
	else :
		save1_button_3.text = "当前无存档"
		await get_tree().create_timer(0.7).timeout
		save1_button_3.text = "删除"

func _file3_save_now():
	save2_button_1.text = "存档中"
	await get_tree().create_timer(1.5).timeout
	SaveAndLoad._file3_save()
	save2_button_1.text = "已存档"
	await get_tree().create_timer(1.0).timeout
	_file3_buttontext_update()

func _file3_load_now():
	if FileAccess.file_exists("user://saving_file_3.txt") and FileAccess.file_exists("user://saving_filetime3.txt"):
		save2_button_2.text = "读取中"
		await get_tree().create_timer(1.0).timeout
		SaveAndLoad._file3_load()
		_chapter_turn()
	else :
		save2_button_2.text = "当前无存档"
		await get_tree().create_timer(1.0).timeout
		save2_button_2.text = "读取"

func _file3_delete_now():
	if FileAccess.file_exists("user://saving_file_3.txt") and FileAccess.file_exists("user://saving_filetime3.txt"):
		save2_button_3.text = "删除中"
		SaveAndLoad._file3_delete()
		await get_tree().create_timer(0.7).timeout
		save2_button_3.text = "已删除"
		_file3_buttontext_update()
		await get_tree().create_timer(0.7).timeout
		save2_button_3.text = "删除"
	else :
		save2_button_3.text = "当前无存档"
		await get_tree().create_timer(0.7).timeout
		save2_button_3.text = "删除"

func _volume_update():
	SaveAndLoad._save_volume_settings()
	volume_change.value = SaveAndLoad._linear_to_db_get(0)
