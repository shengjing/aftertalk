extends Control

var music_player : AudioStreamPlayer = AudioStreamPlayer.new()
@onready var alist : things_list = ResourceLoader.load("res://resuorces/things_list.tres")

func _play_music():
	music_player.finished.connect(func(): music_player.play())
	add_child(music_player)
	
	music_player.stream = alist.music_list[0]
	music_player.play()

